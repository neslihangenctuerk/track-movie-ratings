# General and Implementation Details

2 Views which can be accessed via route links:
    - __Home__
    - __Ratings__

The Ratings view consists of 2 components:
- __AddMovie__: 
    contains Form to add new movie; emits event for adding a Movie; receives prop for calculating a unique ID, which is needed when cycling through the movies list to display each list items content via a Movie component
- __Movie__: 
    component which displays a single movie 


Simple VueJS application which demonstrates basic usage of props & how to prevent mutating them directly as this is bad practice according to the official style guide & how to emit events up to a parent component.
Splitting code into components; proper use of computed vs methods vs data & the lifecycle method __created()__.

Showing&hiding content based on state.

Cycling through an array from state to display Movie information.

Submitting form data & how to handle button clicks in the context of forms.

Basic routing with __Vue__ __Router__.
__LocalStorage__ - so added movies are persisted throughout route changes&restarts.
For first page visits, movies is set to a provided array in the data function, 
upon adding a new movie all movies are saved to localStorage & loaded from localStorage from that point on.

SCSS in Vue.

__Possible next steps__:
- Adding a delete btn.
- Adding images.

Local development environment:
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on font imports before you run the project via this script. The fonts are not crucial to the functionality of the app.


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Tools 
Used [Vue CLI](https://github.com/vuejs/vue-cli/blob/dev/LICENSE). For a copy of the license visit the __NOTICE.md__ file in this project.

---

Used [Vue](https://github.com/vuejs/vue/blob/dev/LICENSE). For a copy of the license visit the __NOTICE.md__ file in this project.

--- 

Used [Vue Router](https://github.com/vuejs/vue-router/blob/dev/LICENSE). For a copy of the license visit the __NOTICE.md__ file in this project.

---

2 Fonts are imported via [Google Fonts](https://fonts.google.com/) - licenses included in each one: 
[Merriweather](https://fonts.google.com/specimen/Merriweather)
[Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro)
or check the __NOTICE.md__ for a direct link.
Check this [Link](https://developers.google.com/fonts/faq) for privacy questions on Font imports before you run the project via the script above. The fonts are not crucial to the functionality of the app.

---

Please also check the __NOTICE.md__ for more info on licenses.